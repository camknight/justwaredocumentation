﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="8" MadCap:lastHeight="848" MadCap:lastWidth="624">
    <head />
    <body>
        <h1 class="tasktitle">Web Service SOAP 1.1 or 1.2 Address Type</h1>
        <div class="prereq">Either <span class="uicontrol">Web Service SOAP 1.1</span> or <span class="uicontrol">Web Service SOAP
				1.2</span> should be selected in the <span class="uicontrol">Address/Transform
				Type</span> field.</div>
        <ol class="steps">
            <li class="step"><span class="cmd">In the Server or File Address Parameters area, complete the following
					fields:</span>
                <div class="info">
                    <ul>
                        <li><span class="uicontrol">Username</span>
                        </li>
                        <li><span class="uicontrol">Password</span>
                        </li>
                        <li><span class="uicontrol">Address</span> The address will typically end in <span class="tt">.asmx</span>. <p>Example: <span class="tt">http://localhost/JustWareWebService/JWWebservice.asmx</span></p></li>
                    </ul>
                </div>
            </li>
            <li class="step" id="QueryToWebMethod"><span class="cmd">After the username, password, and address are entered, click the <span class="uicontrol">Query</span> button to verify the validity of the URI. If the address
					is valid, a list of functions will appear. You can select the appropriate
					function by double-clicking it. Alternatively, you can click the selected
					function and click the <span class="uicontrol">Generate</span> button.</span>
                <div class="info">
                    <div class="note"><b>Note: </b> The <span class="uicontrol">Webservice Method</span> field will automatically
						populate after a function has been selected.</div>
                </div>
            </li>
            <li class="step"><span class="cmd">In the Webservice Parameters area, complete the following fields:</span>
                <div class="info">
                    <ul>
                        <li><span class="uicontrol">Integrated Security:</span> Check this box if you would
							like to enable integrated security so that your username and password
							are impersonated before making the Webservice call.
							</li>
                        <li><span class="uicontrol">Response Status XPath:</span> This XPath locates the
							position in the XML of the status message indicating if a successful
							result was achieved. The XPath should be written in a format similar to
								this:<p><span class="tt">contains (//*[local-name()='name of XML&#160;node with result'], 'success')</span><div class="note"><b>Note: </b>If no Response Status XPath is used, the success is determined based on
									the existence of a Response Message XPath.</div></p></li>
                        <li><span class="uicontrol">Response Message XPath:</span> This XPath locates the
							position in the XML of the response message to be sent to the next
							action. The XPath should be written in a format similar to this:<p><span class="tt">//*[local-name()='name&#160;of XML node with the return message']</span></p></li>
                    </ul>
                </div>
            </li>
            <li class="step"><span class="cmd">The <span class="uicontrol">Request Template</span> field will automatically populate
					after a function has been selected from the list created by the <span class="uicontrol">Query</span> button. However, as it is a template, you will need to
					delete the optional fields that are not needed. Additionally, you will need to
					fill in the input fields: </span>
                <div class="info">
                    <ul>
                        <li><span class="tt">%USERNAME%</span>
                        </li>
                        <li><span class="tt">%FILENAME%</span>
                        </li>
                        <li><span class="tt">%QUEUEID%</span>
                            <p>This field is used if data comes out of the queue
								at some point, and it may have an invalid value.
								</p>
                        </li>
                        <li><span class="tt">%PASSWORD%</span>
                        </li>
                        <li><span class="tt">%EXCHANGECODE%</span>
                            <p>This is the same unique identifier used in the
									<span class="uicontrol">Exchange Entity </span> snap-in.</p>
                        </li>
                        <li><span class="tt">%PAYLOAD%</span>
                            <p>This is the data from the previous step, if
								applicable. For example, if this is part of step 1, the field is not
								necessary.</p>
                        </li>
                    </ul>
                    <div class="noteTip"><b>Tip: </b>You can use the Macros button to automatically insert data for
						the input fields. Using the button for a field will replace the selected
						text.</div>
                </div>
            </li>
            <li>
                <MadCap:snippetBlock src="conrefs_save.flsnp" />
            </li>
        </ol>
    </body>
</html>