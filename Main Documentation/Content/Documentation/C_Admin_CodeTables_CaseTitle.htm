﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="10" MadCap:lastHeight="1945" MadCap:lastWidth="1400" MadCap:conditions="Audience.admin">
    <head>
        <link href="../Resources/TableStyles/FieldDescriptions.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1>Case Title Configuration</h1>
        <p>Each case in JustWare has a case title. This is the name of the case that appears at the top of the case, in the session tab, in hyperlinks to the case, and in search results. It may appear as "Doe, John D. - 132 ~ Seat Belt Violation". Often the case title appears alongside other information (case type, case ID, date, and court numbers are often included in search results) but the case title itself refers to just the pieces of critical information that appear in the session tab and other places.</p>
        <p>You can configure the format of case titles. Each subsequent case entered in JustWare of a case type and agency that has a case title rule configured for it will have its title generated in the format specified. Updating cases of the specified types will also update their case titles. Creating new case title rules will NOT retroactively update existing case titles. (To retroactively update all case titles in the database, see the Maintenance Console Guide (Database section, Rebuild All Case Titles control.)</p>
        <h3>Finding Case Title Configuration</h3>
        <ol class="steps">
            <li class="step">
                <MadCap:snippetText src="../Resources/Snippets/T_Admin_CodeTables_systemadmin.flsnp">&#160;</MadCap:snippetText>
            </li>
            <li>Click <b>Code Tables | Cases |&#160;Case Title Configuration</b> to open the code table.
			</li>
        </ol>
        <div class="postreq">
            <p>
                <div class="noteTip"><b>Tip: </b>
                    <MadCap:snippetText src="../Resources/Snippets/T_Admin_CodeTables_codetables.flsnp">&#160;</MadCap:snippetText><span class="uicontrol">Case Title Configuration</span>.
					</div>
            </p>
        </div>
        <h3>Creating Case Title Rules</h3>
        <p>There are three parts to the Case Title Configuration screen:</p>
        <ul>
            <li>Case Title Configuration code table, located at the top of the screen</li>
            <li>Case Title Parts snap-in, in the middle</li>
            <li>Part Format snap-in, on the bottom</li>
        </ul>
        <h4>Case Title Configuration Code Table</h4>
        <ol class="steps">
            <li class="step">
                <MadCap:snippetBlock src="../Resources/Snippets/JustWare_AddNewRecord.flsnp">&#160;</MadCap:snippetBlock>
            </li>
            <li class="step">
                <MadCap:snippetText src="../Resources/Snippets/conrefs_selectdata.flsnp">&#160;</MadCap:snippetText>
                <table class="TableStyle-FieldDescriptions" style="mc-table-style: url('../Resources/TableStyles/FieldDescriptions.css');margin-left: 0;margin-right: auto;caption-side: top;" cellspacing="0">
                    <col class="Column-Column1" />
                    <col class="Column-Column2" />
                    <thead>
                        <tr class="Head-Header1">
                            <th class="HeadE-Column1-Header1">Field</th>
                            <th class="HeadD-Column2-Header1">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="Body-Body1">
                            <td class="BodyE-Column1-Body1">Title Description</td>
                            <td class="BodyD-Column2-Body1">Type a description for the case title rule that will help you remember what this rule is used for. Example:&#160;Traffic Case Titles</td>
                        </tr>
                        <tr class="Body-Body2">
                            <td class="BodyE-Column1-Body2">Agencies Added By</td>
                            <td class="BodyD-Column2-Body2">Select the agency or agencies that this case title rule should apply to. You can use this drop-down list to exclude members of certain agencies from having their case titles generated according to this rule. The list of agencies only displays those with users assigned to them. A specific agency and case type combination can only be selected for one case title rule.</td>
                        </tr>
                        <tr class="Body-Body1">
                            <td class="BodyE-Column1-Body1">Case Types</td>
                            <td class="BodyD-Column2-Body1">Select the case type or types that this case title rule should apply to. You can use this drop-down list to exclude certain case types from having their case titles generated according to this rule. A specific agency and case type combination can only be selected for one case title rule.</td>
                        </tr>
                        <tr class="Body-Body2">
                            <td class="BodyB-Column1-Body2">Allow Manual Edit</td>
                            <td class="BodyA-Column2-Body2">Select this check box to give users  the ability to manually edit the title of individual cases. <span MadCap:conditions="Audience.online">See <a href="T_User_CreatingACaseSession.htm">Creating a Case Session</a> for details on editing individual case titles.</span> To be able to edit case titles, users must be in a security profile that has the <a href="T_Admin_AddingProfileAttributes.htm">CanEditCaseTitle</a> attribute and have permission to update the Case Information snap-in.</td>
                        </tr>
                    </tbody>
                </table>
            </li>
        </ol>
        <h4>Case Title Parts Snap-in</h4>
        <ol>
            <li>
                <MadCap:snippetBlock src="../Resources/Snippets/JustWare_AddNewRecord.flsnp">&#160;</MadCap:snippetBlock>
            </li>
            <li>In the Part Number field, type a number to dictate the order in which the part will appear in case titles. For example, type 1 for this part to be the first part displayed.</li>
            <li>In the Part Type field, select the type of information to display for this part of the case title.</li>
            <p>
                <table class="TableStyle-FieldDescriptions" style="mc-table-style: url('../Resources/TableStyles/FieldDescriptions.css'); margin-left: 0; margin-right: auto;" cellspacing="0">
                    <col class="Column-Column1" />
                    <col class="Column-Column2" />
                    <thead>
                        <tr class="Head-Header1">
                            <th class="HeadE-Column1-Header1">Field</th>
                            <th class="HeadD-Column2-Header1">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="Body-Body1">
                            <td class="BodyE-Column1-Body1">Default Title (Name ~&#160;Charge)</td>
                            <td class="BodyD-Column2-Body1">
                                <p>Displays a combination of primary involved person and charge, similar to the default JustWare case title format that is auto-generated for all cases that are not included in one of the rules above. This default case title format is generated as follows:</p>
                                <p><b>[Primary Involved Person] ~ [Modifier] ~ [Case Type] [Charge]</b>
                                </p>
                                <p>If multiple primary involved names are on the case, all are listed, separated by commas. Case Type is only displayed if there is no charge. The charge listed is, by default, the charge with the lowest non-zero count number. </p>
                                <p>The Part Format snap-in below allows you to choose more specific charge information to display. </p>
                            </td>
                        </tr>
                        <tr class="Body-Body2">
                            <td class="BodyE-Column1-Body2">Free Text</td>
                            <td class="BodyD-Column2-Body2">Allows you to specify text that will appear exactly as you type it in the Part Format snap-in below.</td>
                        </tr>
                        <tr class="Body-Body1">
                            <td class="BodyE-Column1-Body1">Involved Name</td>
                            <td class="BodyD-Column2-Body1">Displays case involved names of the types specified in the Part Format snap-in below.</td>
                        </tr>
                        <tr class="Body-Body2">
                            <td class="BodyB-Column1-Body2">Lead Number</td>
                            <td class="BodyA-Column2-Body2">Displays the lead number for the Agency Type specified in the Part Format snap-in below.</td>
                        </tr>
                    </tbody>
                </table>
            </p>
            <li>Configure the format the part should be displayed with in the Part Format snap-in (see below).  The information displayed in the Part Format snap-in changes depending on the part type you have selected in the Case Title Parts snap-in.</li>
            <li>Repeat steps 1 through 3 until all the desired parts are included and formatted. <![CDATA[          ]]></li>
        </ol>
        <h4>Part Format Snap-in</h4>
        <p>For each part created in the Case Title Parts snap-in, configure the details of how the part will be displayed. The information displayed in this snap-in changes depending on the part type selected above.</p>
        <table class="TableStyle-FieldDescriptions" style="mc-table-style: url('../Resources/TableStyles/FieldDescriptions.css'); margin-left: 0; margin-right: auto;" cellspacing="0">
            <col style="width: 13px;" class="Column-Column1" />
            <col class="Column-Column2" />
            <thead>
                <tr class="Head-Header1">
                    <th class="HeadE-Column1-Header1">Field</th>
                    <th class="HeadD-Column2-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="Body-Body1">
                    <td class="BodyE-Column1-Body1">Default Title (Name ~&#160;Charge)</td>
                    <td class="BodyD-Column2-Body1">Select the format option that best fits the charge information you want to be displayed.<ul><li><b>Primary Involved Person ~ Charge:</b> Displays the default case title format as described above, using the charge with the lowest non-zero count number.</li><li><b>Primary Involved Person ~&#160;Highest Charge by Charge Type:</b> Same as above, but uses the charge with the highest Charge Type Master Code if there is more than one charge with the lowest non-zero count number. This allows a charge to be displayed based on Charge Type instead of simply the charge with the first count number. For example, if a defendant was apprehended for Armed Robbery (the Arrest Charge) but that was later reduced to Robbery (the Complaint Charge) and later reduced even further to Theft (the Indictment Charge), the Theft charge could be displayed in the case title instead of the Armed Robbery even though the Armed Robbery came first and had the lowest count number.</li><li><b>Primary Involved Person ~&#160;[Charge Type] Highest Charge by Charge Type:</b> Same as above, but displays the Charge Type in brackets and then the charge with the highest Charge Type Master Code.</li></ul></td>
                </tr>
                <tr class="Body-Body2">
                    <td class="BodyE-Column1-Body2">Free Text</td>
                    <td class="BodyD-Column2-Body2">Type text in the box to be displayed in the case title as static text that does not change from case to case. Example:&#160;"Johnson <b>v.</b> Johnson" (the " v. "&#160;is Free Text).</td>
                </tr>
                <tr class="Body-Body1">
                    <td class="BodyE-Column1-Body1">Involved Name</td>
                    <td class="BodyD-Column2-Body1">Select a case involved name to appear in the case title.<ul><li><b>Involvement:</b> Select the specific involvement type to pull from the case. Example:&#160;"Defendant" (displays case involved person of Defendant type)</li><li><b>Maximum Listed:</b> Select the number of involved names to display, if there are more than one of the specified type on the case. Example:&#160;"1" (only shows one name, even if more are on the case)</li><li><b>Separator:</b> Select a character to display in between multiple names, if multiple are displayed. Example:&#160;"," (would appear as "John Doe, Jenny Doe, Jacob Doe")</li><li><b>Truncated List Suffix (i.e., et al):</b> Type text to display at the end of a list of multiple names, if multiple are displayed. Example:&#160;"et al". This only applies when a Maximum Listed other than "All"&#160;is selected and the list of names exceeds that maximum.</li><li><b>Letter Case:</b> Select the casing format for names. Example:&#160;"Capitalize Each Word" (would appear as "John Doe")</li><li><b>Name Format:</b> Select the order and method to display names, and whether initials should be used. Example:&#160;"Last, First M" (would appear as "Doe, John R")</li><li><b>Show Active Only:</b>&#160;Select the box to only display active case involved names. Clear the box to display both active and inactive names.</li></ul></td>
                </tr>
                <tr class="Body-Body2">
                    <td class="BodyB-Column1-Body2">Lead Number</td>
                    <td class="BodyA-Column2-Body2">Select an Agency Type. The case number of the selected Agency Type that is marked Lead and Active on the case will be displayed in the case title.</td>
                </tr>
            </tbody>
        </table>
        <p>
            <MadCap:snippetText src="../Resources/Snippets/Save.flsnp" /> The case title rule you configured is now active, and new cases entered by members of the agencies specified, that are of the case types specified, will have their titles formatted according to the rule you configured. Also, updating any part of a case of the types specified will also update its case title according to the rule you configured.</p>
        <h3>Applying&#160;Case Title Changes to Existing/Past Cases</h3>
        <p>You can apply custom case title rules (or name order changes through the application parameter) to all case titles in the entire database.</p>
        <p>
            <img src="../Resources/Images/Caution.png" />&#160;This procedure could take many hours on a large database and will time out the web service. Plan for a block of time when it will not disrupt the work of JustWare users.</p>
        <ol>
            <li>Open the JustWare Maintenance Console.</li>
            <li>Navigate to the <b>Database</b> tab. </li>
            <li>Select the <b>Rebuild All Case Titles</b> box and click <b>Perform Advanced Options</b>. </li>
        </ol>
    </body>
</html>