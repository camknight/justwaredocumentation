﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" id="R_JDA_SQL" class="reference" MadCap:conditions="Audience.JDA" MadCap:lastBlockDepth="12" MadCap:lastHeight="1889" MadCap:lastWidth="862">
    <head>
        <link href="../Resources/TableStyles/GrayHeaderWithBorders.css" rel="stylesheet" MadCap:stylesheetType="table" /><title>What is SQL?</title>
        <link href="../AdminGuide/Master.css" rel="stylesheet" />
    </head>
    <body class="refbody">
        <h1 class="referencetitle">
            <MadCap:keyword term="SQL" />What is SQL?</h1>
        <div class="section">
            <p>JDA 2.0 generates documents by pulling data from the JustWare database and using it
				to replace certain fillpoints throughout a document. This is accomplished using
				Structured Query Language. Structured Query Language, or SQL (commonly pronounced like the word "sequel"), is a
				database computer language designed for the retrieval and management of data. Queries written in SQL are used to retrieve specific data from tables within a database. </p>
            <p>&#160;</p>
            <h2>Writing Queries in SQL</h2>
            <div class="section">
                <p>Defining a datasource involves writing queries in SQL. These queries tell the editor
				which views to reference in the JustWare database. A datasource is composed of one
				or more query groups, and each query group is composed of one or more related
				queries. Each query calls for one table of information. </p>
                <p>All queries in SQL begin with the <span class="tt">SELECT</span> keyword. <span class="tt">SELECT</span> retrieves
				data from a specified table or tables within a database. For example: <span class="tt">SELECT
					{Define Datasource: YourFieldsHere}</span>. The user can specify the desired
				result of the query by defining certain clauses, which could include:</p>
                <p>
                    <ul>
                        <li><b>FROM:</b> Indicates the source table or tables from which the data is to
						be retrieved.</li>
                        <li><b>WHERE:</b> Includes a comparison predicate that restricts the number of
						rows returned by a query. This clause elminates all rows from the result set
						where the comparison predicate does not evaluate to true. For example, <span class="tt">WHERE {Define Datasource:&#160;YourConditional}</span>.</li>
                        <li><b>GROUP BY:</b> Used to combine rows with related values into elements of a
						smaller set of rows. This clause is often used to eliminate duplicate rows
						from a result set.</li>
                        <li><b>HAVING:</b> Includes a comparison predicate used to eliminate rows after
						the GROUP BY clause is applied to the result set. Because it acts on the
						results of the GROUP BY clause, aggregate functions can be used in the
						HAVING clause predicate. </li>
                        <li><b>ORDER BY:</b> Used to identify which columns are used to sort the
						resulting data, and in which order they should be sorted. (Options are
						ascending or descending.) The order of rows returned by a query in SQL is
						never guaranteed unless an ORDER BY clause is specified.</li>
                    </ul>
                </p>
                <p>The asterisk symbol (*) can be used as a wildcard for queries.</p>
                <p>A model can be used as a guide when building queries.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>
                            <MadCap:snippetText src="../Resources/Snippets/JustWare_ClickForDetails.flsnp" />&#160;</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <table class="TableStyle-GrayHeaderWithBorders" style="mc-table-style: url('../Resources/TableStyles/GrayHeaderWithBorders.css');margin-left: 0;margin-right: auto;caption-side: top;" cellspacing="0">
                            <col class="Column-Column1">
                            </col>
                            <tbody>
                                <tr class="Body-Body1">
                                    <td class="BodyA-Column1-Body1">
                                        <p>A model for building queries:<br /><br /><span class="tt">&lt;DataSource xmlns="http://tempuri.org/XMLSchema1.xsd"&gt;</span></p>
                                        <p><span class="tt">&lt;Queries name="User Defined Queries"&gt;</span>
                                        </p>
                                        <p><span class="tt">&lt;Query name="{Name For Query}" sql="SELECT {Fields From Table} FROM {Table To Query
					From} WHERE {Your Conditional Statement}" /&gt;</span>
                                        </p>
                                        <p><span class="tt">&lt;/Queries&gt;</span>
                                        </p>
                                        <p><span class="tt">&lt;/DataSource&gt;</span>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>
                        </p><b>Caution: </b>The curly brackets <span class="tt">{}</span> indicate where information
						can be added and should not be used as part of your actual query.
		
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
            </div>
            <p>&#160;</p>
            <h3>Creating a Query in SQL</h3>
            <div class="prereq">
                <p>Because defining a datasource involves SQL, it can be quite complicated. There are,
				however, four basic elements in every datasource query:</p>
                <ol>
                    <li><span class="uicontrol">Query name:</span> The name that will appear in the <span class="uicontrol">Fillpoint Pane</span>. This can be any name you desire.</li>
                    <li><span class="uicontrol">Fields:</span> The specific fields being referenced from the
					snap-in.</li>
                    <li><span class="uicontrol">Views:</span> The view name of the snap-in being referenced in
					the template. </li>
                    <li><span class="uicontrol">Conditions:</span> Conditions that limit results to deliver
					specific results. <div class="noteTip"><b>Tip: </b>One condition that should always be specified
						is <span class="tt">@CaseID</span> or <span class="tt">@NameID</span>. This tells the editor to only
						reference information from the database with the Name or Case ID that the
						document is being generated for. The document will still generate without
						this condition, but it will reference any names or cases in the database
						that meet the listed conditions instead of just referencing the desired name
						or case.</div></li>
                </ol>
            </div>
            <MadCap:dropDown>
                <MadCap:dropDownHead>
                    <MadCap:dropDownHotspot>
                        <MadCap:snippetText src="../Resources/Snippets/JustWare_ClickForExample.flsnp">
                        </MadCap:snippetText>&#160;</MadCap:dropDownHotspot>
                </MadCap:dropDownHead>
                <MadCap:dropDownBody>
                    <table class="TableStyle-GrayHeaderWithBorders" style="mc-table-style: url('../Resources/TableStyles/GrayHeaderWithBorders.css');margin-left: 0;margin-right: auto;caption-side: top;" cellspacing="0">
                        <col class="Column-Column1">
                        </col>
                        <tbody>
                            <tr class="Body-Body1">
                                <td class="BodyA-Column1-Body1">
                                    <p>This example describes how to define a datasource that will pull the first and last
				name from the Name snap-in.</p>
                                    <ol>
                                        <li>Create a query name. In this case, Defendant. <dl class="sl"><dt class="sli"><span class="tt">&lt;Query name="Defendant"</span></dt></dl></li>
                                        <li>Select the fields to reference in the document. In this case, the First and Last
					Name fields in the Name snap-in. <dl class="sl"><dt class="sli"><span class="tt">sql="SELECT FirstName, LastName</span></dt></dl></li>
                                        <li>Specify the view name of the snap-in being referenced. In this case, the Name
					snap-in. <dl class="sl"><dt class="sli"><span class="tt">FROM dbo.jw50_Name</span></dt></dl></li>
                                        <li>Specify any conditions you want to use to limit your results. In this case,
					specifying the specific name ID to be referenced. <br></br><br></br><span class="tt">WHERE NameID = @NameID" /&gt;</span></li>
                                    </ol>
                                    <dl class="sl">
                                        <p>
                                            <br />The final SELECT string looks like the following:</p>
                                        <dl class="sl">
                                            <dt class="sli"><span class="tt">&lt;DataSource xmlns="http://tempuri.org/XMLSchema1.xsd"&gt;</span>
                                            </dt>
                                            <dt class="sli"><span class="tt">&lt;Queries name="User Defined Queries"&gt;</span>
                                            </dt>
                                            <dt class="sli"><span class="tt">&lt;Query name="Defendant" sql="SELECT FirstName, LastName FROM
dbo.jw50_Name WHERE NameID = @NameID" /&gt;</span>
                                            </dt>
                                            <dt class="sli"><span class="tt">&lt;/Queries&gt;</span>
                                            </dt>
                                            <dt class="sli"><span class="tt">&lt;/DataSource&gt;</span>
                                            </dt>
                                        </dl>
                                    </dl>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </MadCap:dropDownBody>
            </MadCap:dropDown>
            <p style="font-weight: normal;">&#160;</p>
            <p style="font-weight: normal;">&#160;</p>
            <p style="font-weight: normal;">&#160;</p>
            <p style="font-weight: normal;"><b>Note:</b>&#160;When the <span class="tt">[query].[column]</span> syntax is used, the column must be included in the SELECT string for each of the queries being linked together. <br /></p>
            <MadCap:dropDown>
                <MadCap:dropDownHead>
                    <MadCap:dropDownHotspot>
                        <MadCap:snippetText src="../Resources/Snippets/JustWare_ClickForExample.flsnp" />
                    </MadCap:dropDownHotspot>
                </MadCap:dropDownHead>
                <MadCap:dropDownBody>
                    <table class="TableStyle-GrayHeaderWithBorders" style="mc-table-style: url('../Resources/TableStyles/GrayHeaderWithBorders.css'); margin-left: 0; margin-right: auto;" cellspacing="0">
                        <tbody>
                            <tr class="Body-Body1">
                                <td class="BodyA-Column1-Body1">
                                    <p>An example that uses the <span class="tt">[NameID]</span> column in two queries:</p>
                                    <dl class="sl">
                                        <dt class="sli"><span class="tt">&lt;DataSource xmlns="http://tempuri.org/XMLSchema1.xsd"&gt;</span>
                                        </dt>
                                        <dt class="sli"><span class="tt">&lt;Queries name="Name Information"&gt;</span>
                                        </dt>
                                        <dt class="sli"><span class="tt">&lt;Query name="Query1" sql="SELECT FirstName, LastName, NameID FROM
dbo.jw50_Name WHERE NameID = @NameID"/&gt;</span>
                                        </dt>
                                        <dt class="sli"><span class="tt">&lt;Query name="Query2" sql="SELECT FirstName, FullName, NameID FROM
devName WHERE NameID IN [Query1].[NameID]"/&gt;</span>
                                        </dt>
                                        <dt class="sli"><span class="tt">&lt;/Queries&gt;</span>
                                        </dt>
                                        <dt class="sli"><span class="tt">&lt;/DataSource&gt;</span>
                                        </dt>
                                    </dl>&#160;</td>
                            </tr>
                        </tbody>
                    </table>
                </MadCap:dropDownBody>
            </MadCap:dropDown>
            <p>More advanced queries require more advanced knowledge of SQL. For more information on
				writing queries in SQL, please see <a href="http://www.w3schools.com/sql/default.asp">http://www.w3schools.com/sql/default.asp</a>.</p>
        </div>
    </body>
</html>