﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" id="R_SArch_SSL" class="reference" MadCap:conditions="Audience.sarch" MadCap:lastBlockDepth="4" MadCap:lastHeight="839" MadCap:lastWidth="624">
    <head><title>Security Issues and Secure Sockets Layer (SSL)</title>
        <link href="../AdminGuide/Master.css" rel="stylesheet" />
    </head>
    <body class="refbody">
        <h1 class="referencetitle">Security Issues and Secure Sockets Layer (SSL)</h1>
        <div class="section">
            <h2 class="sectiontitle">Concerns for Sensitive Data</h2>
            <p>New Dawn understands that your organization owns sensitive data that must be
				carefully protected. Consequently, the JustWare line of products and features
				implement security measures to ensure that your data remains secure.</p>
            <p>Consider as an example placing a phone call to someone you’ve never contacted before.
				How can you be sure that the person who answers your call is really the person you
				are trying to reach? How would you know if your call has been re-routed? Even if you
				do reach the correct person, how do you ensure that no one is tapped into your line
				and listening to your conversation?</p>
            <p>In today’s high-tech industry, matters of security are of utmost importance.
				Transmitting data across a network is a justifiable cause for concern, and sending
				sensitive information over the Internet only increases that level of concern.
				Questions loom: How can I protect my information? How can I verify that I am
				communicating with the server I think I am? How can I make sure my data does not
				become corrupted? How can I ensure that unauthorized parties do not access my
				information?</p>
            <p>These questions are both appropriate and valid. No one wants to be a victim.</p>
            <p>New Dawn has implemented encryption security into JustWare’s products and features to
				maintain the integrity of your sensitive data. An explanation of the layers of this
				security is provided in the following sections.</p>
        </div>
        <div class="section">
            <h2 class="sectiontitle">Encryption</h2>
            <p>A practical method of securing data for sharing is to utilize encryption. Encryption
				is the process of encoding information in such a way that only the authorized
				recipient with the correct key can decode it. Computer encryption is based on the
				science of cryptography, which utilizes algorithms and keys. An algorithm can be
				thought of as the process for encoding the data, and the key can be considered the
				means for decoding the data.</p>
            <p>Encryption processes fall into two categories: symmetric and public.</p>
            <h2 class="sectiontitle">Symmetric Key Encryption</h2>
            <p>For symmetric key encryption to work, both parties must share the same key to send
				information between each other. For example, a computer uses a key to encrypt data.
				The data is sent over a network to another computer. The receiving computer uses the
				same key as the sending computer to decrypt the data. This method of encryption is
				not considered very secure, as outside sources could, through trying every possible
				symmetric algorithm, decipher encrypted data in a short period of time. Moreover,
				since a computer at each end of the transmission is using the same key, it is easier
				for an attacker to identify the key.</p>
            <h2 class="sectiontitle">Public Key Encryption</h2>
            <p>Public key encryption was developed to address the weaknesses of symmetric key
				encryption. Public key encryption utilizes two different keys: a private key and a
				public key. The public key is released by your computer to any computer that wants
				to communicate securely with it. The private key, on the other hand, is only known
				to your computer. In order for a computer to decode an encrypted message, that
				computer must use the public key from the originating computer as well as its own
				private key.</p>
            <p>The use of a public key alone does not guarantee security, as it is available to
				anyone, and anyone can pick up the data. However, the data is unintelligible without
				the unique private key for decryption. Consequently, only the authorized recipient
				with the private key can read the data.</p>
            <p>To better conceptualize how public key encryption works, imagine renting a Post
				Office Box. Like a public key, the address of your PO Box is made public so that
				anyone who wishes to may send mail to it. Even though anyone can send mail to your
				PO Box, only you have the private key to open the PO Box and retrieve and read the
				mail.</p>
            <div class="fig"><span class="figtitle">Receiving messages that have been encrypted with your organization's public
					key is like renting a PO Box from the Post Office. While anyone can send mail to
					you, only you have the key that allows you to retrieve and read the
					mail.</span>
                <p>
                    <img src="../Resources/Images/POBox.gif" style="text-align: center;width: 299px;height: 179px;" />
                </p>
            </div>
            <p>Public key encryption also works in the opposite direction. With your private key,
				you can encrypt a message. This is similar to signing a letter before a Notary
				Public. The Notary Public certifies that you are the signer of the letter, and no
				one else can say that they are the originator of the letter. The notarized letter,
				however, can be decrypted using your public key, and anyone can read the letter.</p>
            <div class="fig"><span class="figtitle">Like having a letter notarized, encrypting a message with your
					organization's private key allows you to prove that you are the actual sender of
					an electronic message.</span>
                <p>
                    <img src="../Resources/Images/NotarizedDocument.gif" style="text-align: center;" />
                </p>
            </div>
        </div>
        <div class="section">
            <h2 class="sectiontitle">Digital Certificates and Certificate Authorities (CAs)</h2>
            <p>The security of public key encryption is further enhanced by the use of digital
				certificates. A digital certificate is a unique piece of code or a large number that
				verifies that the server is trusted by an independent third party known as a
				Certificate Authority (CA). The CA issues the digital certificate and acts as a
				trusted intermediary between the two computers that wish to transmit data. The CA
				confirms that each computer is in fact who it says it is and provides the public
				keys of each computer to the other. Essentially, a digital certificate establishes a
				private communication channel between the two computers.</p>
        </div>
        <div class="section">
            <h2 class="sectiontitle">Secure Sockets Layer (SSL)</h2>
            <p>An implementation of public key encryption that utilizes digital certificates is
				Secure Sockets Layer (SSL). An example of SSL at work is when you log into your
				bank’s Web site to do some online banking. The action of navigating from your bank’s
				homepage to the logon page changes your bank’s Web address in your browser’s address
				line from “http://” to “https://.” Additionally, a small padlock appears in the
				status bar at the bottom of the browser window. These are two indications that your
				username and password are being transmitted securely.</p>
            <p>Once your browser requests a secure page and adds the “s” to “http,” the browser
				sends out the public key and the digital certificate. Three things are checked: that
				the digital certificate comes from a trusted party, that the certificate is
				currently valid, and that the certificate has a relationship with the site from
				which it is coming.</p>
            <p>The browser uses the public key to encrypt a symmetric key that is randomly selected.
				Because public key encryption takes a lot of computing, most systems use a
				combination of public key and symmetric key encryption.</p>
            <p>When two computers initiate a secure session, the sending computer encrypts the data
				to be transmitted with a symmetric key and sends it with the public key of the
				receiving computer to the receiving computer. The receiving computer uses its
				private key to decode the symmetric key. Then the receiving computer uses the
				symmetric key to decode the data. The two computers then communicate using symmetric
				key encryption. When the session is finished, each computer discards the symmetric
				key used for that session. If these two computers would like to initiate another
				secure session, then a new symmetric key would be created, and the process would be
				repeated.</p>
        </div>
        <div class="section">
            <h2 class="sectiontitle">Authentication and Digital Signatures on Digital Certificates</h2>
            <p>In addition to the use of keys, the security of public key encryption is further
				enhanced by the addition of another process: authentication. Simply put,
				authentication is used to verify the source of the data being transmitted and to
				verify that the data has not been tampered with by an unauthorized party.</p>
            <p>The type of authentication used with SSL is a digital signature on a digital
				certificate. A digital signature is a means by which to ensure that transmitted data
				is authentic. A private key, known only by the originator of the transmitted data,
				and a public key are used. If anything is changed in the data after the digital
				signature is attached to it, then the value used to compare the digital signature is
				changed, and the signature becomes invalid.</p>
            <p>The SSL’s digital certificate (SSL Certificate) typically contains the server name,
				information regarding the Certificate Authority (CA), and the server’s public
				encryption key, as well as a private key only known to the recipient of the message.
				The public key is used to encrypt information, and the private key is used to
				decipher it. The client uses the public key to validate the CA’s digital signature
				on the SSL Certificate. The client verifies that the issuing CA is on its list of
				trusted CAs. If the digital signature can be verified, then the client accepts the
				SSL Certificate as a valid certificate issued by a trusted CA, and the digital
				signature on the certificate is viewed as attesting that the server identity and
				public key are indeed correctly linked. The client also checks the server’s
				certificate validity period to ensure that the current date and time fall within the
				validity period.</p>
        </div>
        <div class="section">
            <h2 class="sectiontitle">JustWare Products and SSL</h2>
            <p>JustWare’s products and features either automatically utilize SSL or offer the option
				to select SSL encryption. When provided with the option to choose SSL encryption, it
				is recommended that you make use of this offered added layer of security. Using SSL
				with digital signatures is a secure method of sending information over a network,
				prevents you from being a victim, and provides you with greater peace of mind
				regarding the integrity of your sensitive data.</p>
        </div>
        <div class="section">
            <h2 class="sectiontitle">Using SSL</h2>
            <p>Using SSL adds a layer of encryption to packets that routers read, and you may wonder how much this process will slow down your system. New Dawn tested JustWare with SSL enabled, and the performance level was not negatively impacted. In fact, in some instances, the performance level was increased by using SSL.</p>
        </div>
        <div class="section">
            <h2 class="sectiontitle">Purchasing SSL Certificates</h2>
            <p>In order to utilize SSL Certificates, this service must be purchased through a CA.
				Many CAs offer digital certificates, each with various certificate products that
				provide different levels of authentication. Generally speaking, the higher the level
				of authentication that is provided (i.e., the more authentication checks used), the
				greater the quality of the certificate. It is recommended to purchase a high-quality
				certificate from an established CA. Certificate products are purchased for terms,
				typically one or two years. </p>
        </div>
    </body>
</html>